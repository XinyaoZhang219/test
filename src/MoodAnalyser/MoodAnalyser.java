package MoodAnalyser;

/**
 * Created by zhangxinyao on 11/15/16.
 */
public class MoodAnalyser {

    public String analysemood(String message) {
        if (message.contains("sad")){
            return "SAD";
        }else
            return "HAPPY";
    }
}
