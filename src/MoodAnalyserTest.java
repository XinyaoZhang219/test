import MoodAnalyser.MoodAnalyser;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by zhangxinyao on 11/15/16.
 */
public class MoodAnalyserTest {
    @Test
    public void testMoodAnalysis() throws Exception{
        MoodAnalyser moodAnalyser = new MoodAnalyser();
        String mood = moodAnalyser.analysemood("this is a  message ");
        Assert.assertThat(mood, CoreMatchers.is("SAD"));
    }
    @Test
    public void testHappyMoods() throws Exception{
        MoodAnalyser moodAnalyser = new MoodAnalyser();

        String mood = moodAnalyser.analysemood("this is a happy message");
        Assert.assertThat(mood,CoreMatchers.is("HAPPY"));
    }

}
